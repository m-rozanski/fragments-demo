package com.rozanski.swim_lab_2

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class Fragment1 : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment1_lay, container, false)

        val btnLoadText: Button = view.findViewById(R.id.btn_loadText)
        val btnLoadImage: Button = view.findViewById(R.id.btn_loadImage)

        btnLoadText.setOnClickListener {
            val textFragment = TextFragment()
            val manager = childFragmentManager
            val transaction = manager.beginTransaction()
            transaction.replace(R.id.fragment_container, textFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }

        btnLoadImage.setOnClickListener {
            val imageFragment = ImageFragment()
            val manager = childFragmentManager
            val transaction = manager.beginTransaction()
            transaction.replace(R.id.fragment_container, imageFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }

        return view
    }
}