package com.rozanski.swim_lab_2

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast


class MyDialogFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_dialog, container, false)
        val btnAccept = view.findViewById<Button>(R.id.buttonAccept)
        val btnCancel = view.findViewById<Button>(R.id.buttonCancel)
        val editText = view.findViewById<EditText>(R.id.editText)

        btnAccept.setOnClickListener {
            var str = editText.text.toString()
            Toast.makeText(activity, str, Toast.LENGTH_SHORT).show()
            dismiss()
        }

        btnCancel.setOnClickListener { dismiss() }
        return view
    }
/*
    companion object {

        internal fun newInstance(num: Int): MyDialogFragment {
            val f = MyDialogFragment()
            val args = Bundle()
            args.putInt("num", num)
            f.arguments = args

            return f
        }
    }*/
}