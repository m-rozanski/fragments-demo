Fragments Demo
====================

It's a simple demo project for studies, just to familiarize with:

* Fragments
* ViewPager
* Dialogs
* showing images from Internet using [Picasso][picasso] and [Glide][glide]

[picasso]: https://square.github.io/picasso/
[glide]: https://github.com/bumptech/glide