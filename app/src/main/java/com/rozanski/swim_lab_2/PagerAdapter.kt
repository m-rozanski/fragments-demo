package com.rozanski.swim_lab_2

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> Fragment1()
        1 -> Fragment2()
        else -> Fragment3()
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence = when (position) {
        0 -> "Fragments"
        1 -> "Dialogs"
        else -> "Images"
    }
}