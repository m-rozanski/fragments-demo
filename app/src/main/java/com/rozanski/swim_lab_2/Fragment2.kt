package com.rozanski.swim_lab_2

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment2_lay.*
import java.util.*

class Fragment2 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment2_lay, container, false)

        val btnAlertDialog: Button = view.findViewById(R.id.btn_alertDialog)
        btnAlertDialog.setOnClickListener {
            val builder: AlertDialog.Builder? = activity?.let {
                AlertDialog.Builder(it)
            }

            with(builder!!) {
                val items = arrayOf("S", "W", "I", "M")
                val selectedList = ArrayList<Int>()

                setMultiChoiceItems(items, null) { _, which, isChecked ->
                    if (isChecked) {
                        selectedList.add(which)
                    } else if (selectedList.contains(which)) {
                        selectedList.remove(Integer.valueOf(which))
                    }
                }

                setTitle(R.string.fragment2)

                setPositiveButton(android.R.string.ok) { dialog, _ ->
                    selectedList.sort()
                    var str = ""
                    for (j in selectedList.indices) {
                        str += items[selectedList[j]]
                    }
                    Toast.makeText(activity, str, Toast.LENGTH_SHORT).show()
                    dialog.dismiss()
                }

                setNeutralButton(android.R.string.cancel) { dialog, _ -> dialog.cancel() }

            }

            val dialog: AlertDialog? = builder.create()
            dialog?.show()
        }

        val btnDialogFragment = view.findViewById<Button>(R.id.btn_dialogFragment)
        btnDialogFragment.setOnClickListener {
            val ft = childFragmentManager.beginTransaction()
            // val newFragment = MyDialogFragment.newInstance(0)
            val newFragment = MyDialogFragment()
            newFragment.show(ft, "dialog")
        }

        val btnDate = view.findViewById<Button>(R.id.btn_Date)
        btnDate.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val dpd =
                DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    txtDate.setText("" + dayOfMonth + "/" + monthOfYear + "/" + year)
                }, year, month, day)

            dpd.show()
        }

        return view
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("date", txtDate.text.toString())
        super.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        txtDate.text = savedInstanceState?.getString("date", "Date")
    }

}