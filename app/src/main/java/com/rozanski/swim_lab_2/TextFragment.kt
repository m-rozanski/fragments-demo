package com.rozanski.swim_lab_2

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast


class TextFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_text, container, false)

        val tv = view.findViewById<TextView>(R.id.txt)
        tv.setOnClickListener {
            tv.setTextColor(Color.RED)
            Toast.makeText(view.context, getString(R.string.textClickedToast), Toast.LENGTH_SHORT).show()
        }
        return view
    }
}
