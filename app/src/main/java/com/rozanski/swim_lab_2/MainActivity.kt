package com.rozanski.swim_lab_2

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setHomeButtonEnabled(true)

        val adapter = PagerAdapter(supportFragmentManager)
        pager.adapter = adapter
        tabs.setupWithViewPager(pager)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_bar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_home -> {
                Toast.makeText(this, getString(R.string.goHome), Toast.LENGTH_SHORT).show()
                pager.setCurrentItem(0, true)
                return true
            }

            R.id.action_2 -> {
                Toast.makeText(this, getString(R.string.action2Clicked), Toast.LENGTH_SHORT).show()
                return true
            }

            R.id.submenu_action -> {
                Toast.makeText(this, getString(R.string.submenuActionClicked), Toast.LENGTH_SHORT).show()
                return true
            }

            R.id.action_4 -> {
                Toast.makeText(this, getString(R.string.action4Clicked), Toast.LENGTH_SHORT).show()
                return true
            }

            else -> return super.onOptionsItemSelected(item)
        }
    }


}
