package com.rozanski.swim_lab_2

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso

private const val IMAGE1URL = "https://www.akc.org/wp-content/themes/akc/component-library/assets/img/welcome.jpg"
private const val IMAGE2URL = "https://assets.rbl.ms/18752379/980x.jpg"

class Fragment3 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment3_lay, container, false)

        val image1: ImageView = view.findViewById(R.id.img1)
        Picasso.get().load(IMAGE1URL).into(image1)

        val image2: ImageView = view.findViewById(R.id.img2)
        Glide.with(this).load(IMAGE2URL).into(image2)

        return view
    }
}